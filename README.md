# README #

## VERSION INFORMATION
- - - - - - - - - - - - - - - - - - - -
Version 1.0 

## INTRODUCTION
- - - - - - - - - - - - - 
The MyPersonalAccountant application helps recording and analysing your daily income and payment, based on the bank records. You can also add the items by typing in individually.

## REQUIREMENTS
- - - - - - - - - - - - - -
* Version 1.0 is only for the Mac user and Handelsbanken’s account holder.
* To import the bank account, copy the bank account items to TextEdit as .txt file.

## TROUBLESHOOTING
- - - - - - - - - - - - - - - - -
* Wrongly display the account items.
Make sure your bank account is from Handelsbanken. 
And the bank account should be copied and maintained unmodified to TextEdit as a .txt file.

* Can’t add items by hand.
Make sure the date and amount area is not empty.
Follow the format of the hint text.

* Can’t display the analysis result.
Make sure the last column (Category) is not empty. The first column (Date) should follow the format yyyy-MM-dd, and the third column (Amount) should be numbers.

## AUTHOR & CONTACTS
- - - - - - - - - - - - - - - - - - 
Yustina Tian
yustina.yutian@gmail.com