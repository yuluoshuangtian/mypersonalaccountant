package com.home.MyPersonalAccountant.view;

import java.awt.Color;
//import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

public class ResultPane extends JPanel{

	private static final long serialVersionUID = 1L;
	
	private JLabel sumAllTextLabel;
	private JLabel sumAllNumberLable;
	private JLabel sumExpenseTextLabel;
	private JLabel sumExpenseNumberLabel;
	private JLabel spaceLabel;
	private JLabel pieChartTextLabel;
	private JLabel barChartTextLabel;
	
	private JFreeChart chartAnalyzeByWhatfor;
	private DefaultPieDataset pieData;
	private ChartPanel pieChartPanel;
	private Plot pieChartPlot;
	
	private JFreeChart chartAnalyzeByMonth;
	private DefaultCategoryDataset barData;
	private ChartPanel barChartPanel;
	private CategoryPlot barChartPlot;
	private BarRenderer barRenderer;
		
	public ResultPane(){
		this.setBackground(Color.WHITE);
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
	
		sumAllTextLabel = new JLabel("  The balance of the chosen period is:");
		sumAllTextLabel.setFont(sumAllTextLabel.getFont().deriveFont(15f));
		sumAllNumberLable = new JLabel();
		sumAllNumberLable.setFont(sumAllNumberLable.getFont().deriveFont(15f));
		sumExpenseTextLabel = new JLabel("  The total spent money of the chosen period is:");
		sumExpenseTextLabel.setFont(sumExpenseTextLabel.getFont().deriveFont(15f));
		sumExpenseNumberLabel = new JLabel();
		sumExpenseNumberLabel.setFont(sumExpenseNumberLabel.getFont().deriveFont(15f));
		spaceLabel = new JLabel(" ");
		
		pieChartTextLabel = new JLabel("  Catecories of spent money:");
		pieChartTextLabel.setFont(pieChartTextLabel.getFont().deriveFont(15f));
		pieData = new DefaultPieDataset();
		chartAnalyzeByWhatfor = createPieChart(pieData);
		pieChartPanel = new ChartPanel(chartAnalyzeByWhatfor);
		pieChartPanel.setLayout(new FlowLayout());
		//pieChartPanel.setPreferredSize(new Dimension(400, 300));
		
		barChartTextLabel = new JLabel("  Monthly spent comparison:");
		barChartTextLabel.setFont(barChartTextLabel.getFont().deriveFont(15f));
		barData = new DefaultCategoryDataset();
		chartAnalyzeByMonth = createBarChart(barData);
		barChartPanel = new ChartPanel(chartAnalyzeByMonth);
		//barChartPanel.setPreferredSize(new Dimension(400, 400));
		
		gl.setHorizontalGroup(gl.createParallelGroup()
				.addComponent(spaceLabel)
				.addComponent(sumAllTextLabel)
				.addComponent(sumAllNumberLable)
				.addComponent(spaceLabel)
				.addComponent(sumExpenseTextLabel)
				.addComponent(sumExpenseNumberLabel)
				.addComponent(spaceLabel)
				.addComponent(pieChartTextLabel)
				.addComponent(pieChartPanel)
				.addComponent(spaceLabel)
				.addComponent(barChartTextLabel)
				.addComponent(barChartPanel));
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addComponent(spaceLabel)
				.addComponent(sumAllTextLabel)
				.addComponent(sumAllNumberLable)
				.addComponent(spaceLabel)
				.addComponent(sumExpenseTextLabel)
				.addComponent(sumExpenseNumberLabel)
				.addComponent(spaceLabel)
				.addComponent(pieChartTextLabel)
				.addComponent(pieChartPanel)
				.addComponent(spaceLabel)
				.addComponent(barChartTextLabel)
				.addComponent(barChartPanel));
		
		sumAllTextLabel.setSize(getPreferredSize());
		sumAllNumberLable.setSize(getPreferredSize());
		sumExpenseTextLabel.setSize(getPreferredSize());
		sumExpenseNumberLabel.setSize(getPreferredSize());
	}
	
	private JFreeChart createPieChart(DefaultPieDataset pieData) {
		JFreeChart pieChart = ChartFactory.createPieChart("", pieData, false, true, true);
		pieChartPlot = pieChart.getPlot();
		pieChartPlot.setNoDataMessage("Please select items.");
		pieChartPlot.setNoDataMessageFont(getFont().deriveFont(15f));
		pieChartPlot.setBackgroundPaint(Color.LIGHT_GRAY);
		pieChartPlot.setOutlineVisible(false);
		
		return pieChart;
	}
	
	private JFreeChart createBarChart(DefaultCategoryDataset barData){
		JFreeChart barChart = ChartFactory.createBarChart("", "Month", "Amount/kr", barData);
		barChartPlot = barChart.getCategoryPlot();
		barChartPlot.setBackgroundPaint(Color.LIGHT_GRAY);
		barChartPlot.setDomainGridlinePaint(Color.WHITE);
		barChartPlot.setOutlineVisible(false);
		
		barRenderer = (BarRenderer)barChartPlot.getRenderer();
		barRenderer.setDrawBarOutline(false);
		barRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		barRenderer.setBaseItemLabelsVisible(true);
		return barChart;
	}
	
	public DefaultPieDataset getPieData(){
		return pieData;
	}
	
	public DefaultCategoryDataset getBarData(){
		return barData;
	}
	
	public JLabel getSumAllNumberLabel(){
		return sumAllNumberLable;
	}
	
	public JLabel getSumExpenseNumberLabel(){
		return sumExpenseNumberLabel;
	}

}
