package com.home.MyPersonalAccountant.view;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.home.MyPersonalAccountant.controller.ToolsPanelController;

public class ToolsPanelView extends JPanel{
	
	private static final long serialVersionUID = 1L;

	private ApplicationFrameWindow applicationFrameWindow;
		
	private JButton btnImportAccountBook;
	private JButton btnAddNewItems;
	private JButton btnDeleteItems;
	private JButton btnAnalyzeSelectedItems;
	private ToolsPanelController toolsPanelController;
	
	public ToolsPanelView(ApplicationFrameWindow applicationFrameWindow){
		
		this.applicationFrameWindow = applicationFrameWindow;
		toolsPanelController = new ToolsPanelController(this);
		
		btnImportAccountBook = new JButton("Import");
		btnImportAccountBook.addActionListener(toolsPanelController);
		add(btnImportAccountBook);
		
		btnAddNewItems = new JButton("Add");
		btnAddNewItems.addActionListener(toolsPanelController);
		add(btnAddNewItems);
		
		btnDeleteItems = new JButton("Delete");
		btnDeleteItems.addActionListener(toolsPanelController);
		add(btnDeleteItems);
		
		btnAnalyzeSelectedItems = new JButton("Analyze");
		btnAnalyzeSelectedItems.addActionListener(toolsPanelController);
		add(btnAnalyzeSelectedItems);
	}
	
	public ApplicationFrameWindow getApplicationFrameWindow(){
		return this.applicationFrameWindow;
	}

}
