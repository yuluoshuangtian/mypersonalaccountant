package com.home.MyPersonalAccountant.view;

import javax.swing.Box;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.home.MyPersonalAccountant.controller.TopMenuController;

public class TopMenuView extends JMenuBar{
	
	private static final long serialVersionUID = 1L;

	private JMenu aboutMenu;
	private JMenuItem menuItemAbout;
	private JMenuItem menuItemHelp;
	private TopMenuController topMenuController;
	
	public TopMenuView(){
		
		topMenuController = new TopMenuController();
		
		aboutMenu = new JMenu("About");
		menuItemAbout = new JMenuItem("About");
		menuItemAbout.addActionListener(topMenuController);
		menuItemHelp = new JMenuItem("Help");
		menuItemHelp.addActionListener(topMenuController);
		
		aboutMenu.add(menuItemAbout);
		aboutMenu.add(menuItemHelp);
		add(Box.createHorizontalGlue());
		add(aboutMenu);	
	}

}
