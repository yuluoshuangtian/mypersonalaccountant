package com.home.MyPersonalAccountant.view;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class ApplicationFrameWindow extends JFrame{
	
	private static final long serialVersionUID = 1L;
	
	private TopMenuView topMenuView;
	private ToolsPanelView toolsPanelView;
	private MainPanelView mainPanelView;
	private MainPanelTable mainPanelTable;
	
	public ApplicationFrameWindow(String title, int width, int height){
		
		this.setTitle(title);
		this.setSize(width, height);
		// initial location at center of screen
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		topMenuView = new TopMenuView();
		setJMenuBar(topMenuView);
		toolsPanelView = new ToolsPanelView(this);
		add(toolsPanelView, BorderLayout.NORTH);
		mainPanelTable = new MainPanelTable();
		add(mainPanelTable, BorderLayout.WEST);
		mainPanelView = new MainPanelView();
		add(mainPanelView, BorderLayout.CENTER);
		
	}
	
	
	public MainPanelTable getMainPanelTable(){
		return this.mainPanelTable;
	}
	public MainPanelView getMainPanelView(){
		return this.mainPanelView;
	}
	
}
