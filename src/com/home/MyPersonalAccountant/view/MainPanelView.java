package com.home.MyPersonalAccountant.view;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EtchedBorder;

public class MainPanelView extends JScrollPane {

	private static final long serialVersionUID = 1L;
	
	private JLabel initText;

	public MainPanelView() {
		this.getViewport().setBackground(Color.WHITE);
		this.setViewportBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		initText = new JLabel("<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please choose the items and press Analyze button.<br><br><br><br><br></html>");
		this.getViewport().add(initText);
		initText.setPreferredSize(getPreferredSize());
	}

	public void showResult(JPanel resultPane) {
		this.getViewport().removeAll();
		this.repaint();
		this.getViewport().add(resultPane);
		this.revalidate();		
	}
	
}
