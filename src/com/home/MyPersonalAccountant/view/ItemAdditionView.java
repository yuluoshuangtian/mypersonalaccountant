package com.home.MyPersonalAccountant.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.Box;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.home.MyPersonalAccountant.controller.ItemAdditionController;
import com.home.MyPersonalAccountant.model.Bank;

public class ItemAdditionView extends JDialog {

	private static final long serialVersionUID = 1L;

	private static final String chooseBankString = "Add items from bank record";
	private static final String addItemByHandString = "Or add item by hand";
	private static final String dateLabelString = "Date";
	private static final String dateFieldHintString = "yyyy-MM-dd";
	private static final String categoryLabelString = "Category";
	private static final String amountLabelString = "Amount";
	private static final String amountFieldHintString = " (-) stands for expenditure";

	private final JTextField dateField = new HintTextField(dateFieldHintString);
	private final JTextField categoryField = new JTextField();
	private final JTextField amountField = new HintTextField(amountFieldHintString);

	private final JLabel chooseBankLabel = new JLabel(chooseBankString + ": ");
	private final JLabel addItemByHandLabel = new JLabel(addItemByHandString + ": ");
	private final JLabel dateFieldLabel = new JLabel(dateLabelString + ": ");
	private final JLabel categoryFieldLabel = new JLabel(categoryLabelString + ": ");
	private final JLabel amountFieldLabel = new JLabel(amountLabelString + ": ");
	private final JLabel spaceLabel = new JLabel("  ");
	
	private final JComboBox<Bank> bankChooser = new JComboBox<Bank>(Bank.values());

	private final JButton okButton = new JButton("OK");
	private final JButton cancelButton = new JButton("Cancel");

	private ItemAdditionController itemAdditionController;
	private ToolsPanelView toolsPanelView;

	public ItemAdditionView(ToolsPanelView toolsPanelView) {
		this.toolsPanelView = toolsPanelView;

		okButton.setEnabled(false);
		
		//bankChooser.setModel(new DefaultCom)

		itemAdditionController = new ItemAdditionController(this);
		dateField.addActionListener(itemAdditionController);
		categoryField.addActionListener(itemAdditionController);
		amountField.addActionListener(itemAdditionController);
		bankChooser.addActionListener(itemAdditionController);
		okButton.addActionListener(itemAdditionController);
		cancelButton.addActionListener(itemAdditionController);

		JPanel labelPane = new JPanel();
		GroupLayout glLabelPane = new GroupLayout(labelPane);
		labelPane.setLayout(glLabelPane);

		glLabelPane.setAutoCreateContainerGaps(true);
		glLabelPane.setAutoCreateGaps(true);

		glLabelPane.setHorizontalGroup(glLabelPane.createParallelGroup()
				.addComponent(chooseBankLabel)
				.addGroup(glLabelPane.createSequentialGroup()
						.addComponent(spaceLabel)
						.addComponent(bankChooser))
				.addGap(10)
				.addComponent(addItemByHandLabel)
				.addGap(10)
				.addGroup(glLabelPane.createSequentialGroup()
						.addGroup(glLabelPane.createParallelGroup()
								.addComponent(spaceLabel)
								.addComponent(spaceLabel)
								.addComponent(spaceLabel))
						.addGroup(glLabelPane.createParallelGroup()
								.addComponent(dateFieldLabel)
								.addComponent(categoryFieldLabel)
								.addComponent(amountFieldLabel))
						.addGroup(glLabelPane.createParallelGroup()
								.addComponent(dateField)
								.addComponent(categoryField)
								.addComponent(amountField))));
				
		glLabelPane.setVerticalGroup(glLabelPane.createSequentialGroup()
				.addComponent(chooseBankLabel)
				.addGroup(glLabelPane.createParallelGroup()
						.addComponent(spaceLabel)
						.addComponent(bankChooser))
				.addGap(10)
				.addComponent(addItemByHandLabel)
				.addGap(10)
				.addGroup(glLabelPane.createParallelGroup()
						.addComponent(spaceLabel)
						.addComponent(dateFieldLabel)
						.addComponent(dateField))
				.addGroup(glLabelPane.createParallelGroup()
						.addComponent(spaceLabel)
						.addComponent(categoryFieldLabel)
						.addComponent(categoryField))
				.addGroup(glLabelPane.createParallelGroup()
						.addComponent(spaceLabel)
						.addComponent(amountFieldLabel)
						.addComponent(amountField)));

		JPanel buttonPane = new JPanel(new FlowLayout());
		buttonPane.add(okButton);
		buttonPane.add(Box.createHorizontalStrut(10));
		buttonPane.add(cancelButton);

		add(labelPane, BorderLayout.NORTH);
		add(buttonPane, BorderLayout.SOUTH);
		pack();

		setTitle("Add item to accountbook table");
		setSize(350, 240);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public JButton getOkButton() {
		return okButton;
	}

	public JButton getCancelButton() {
		return cancelButton;
	}

	public JTextField getDateField() {
		return dateField;
	}

	public JTextField getCategoryField() {
		return categoryField;
	}

	public JTextField getAmountField() {
		return amountField;
	}

	public ToolsPanelView getToolsPanelView() {
		return toolsPanelView;
	}
}

class HintTextField extends JTextField implements FocusListener {

	private static final long serialVersionUID = 1L;

	private final String hint;
	private boolean showingHint;

	public HintTextField(final String hint) {
		super(hint);
		this.hint = hint;
		this.showingHint = true;
		this.setForeground(Color.LIGHT_GRAY);
		super.addFocusListener(this);
	}

	@Override
	public void focusGained(FocusEvent e) {
		if (this.getText().isEmpty()) {
			super.setText("");
			this.setForeground(Color.BLACK);
			showingHint = false;
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		if (this.getText().isEmpty()) {
			super.setText(hint);
			this.setForeground(Color.LIGHT_GRAY);
			showingHint = true;
		}
	}

	@Override
	public String getText() {
		return showingHint ? "" : super.getText();
	}
}