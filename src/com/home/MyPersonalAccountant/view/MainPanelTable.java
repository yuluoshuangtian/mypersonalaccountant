package com.home.MyPersonalAccountant.view;

import java.awt.Color;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.home.MyPersonalAccountant.model.AccountBookTableModel;
import com.home.MyPersonalAccountant.model.Category;

public class MainPanelTable extends JScrollPane {
	private static final long serialVersionUID = 1L;

	private AccountBookTableModel tableModel;
	private JTable accountBookTable;
	private TableRowSorter<TableModel> rowSorter;

	public MainPanelTable() {
		super();
		this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		tableModel = new AccountBookTableModel();
		accountBookTable = new JTable(tableModel);

		rowSorter = new TableRowSorter<TableModel>(tableModel);
		accountBookTable.setRowSorter(rowSorter);
		rowSorter.setComparator(0, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				try {
					Date date1 = dateFormat.parse(o1);
					Date date2 = dateFormat.parse(o2);
					return date1.compareTo(date2);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return o1.compareTo(o2);
			}

		});
		rowSorter.setComparator(2, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				String number1 = o1.replaceAll(",", ".").replaceAll(" ", "");
				String number2 = o2.replaceAll(",", ".").replaceAll(" ", "");
				double amount1 = Double.parseDouble(number1);
				double amount2 = Double.parseDouble(number2);
				return Double.compare(amount1, amount2);
			}
		});

		setUpCategoryColumn(accountBookTable, accountBookTable.getColumnModel().getColumn(3));
		accountBookTable.setRowSelectionAllowed(true); // default to be true
		accountBookTable.setColumnSelectionAllowed(false);

		this.getViewport().add(accountBookTable);
		this.getViewport().setBackground(Color.WHITE);
	}

	private void setUpCategoryColumn(JTable table, TableColumn categoryColumn) {
		JComboBox<Category> comboBox = new JComboBox<Category>(Category.values());
		categoryColumn.setCellEditor(new DefaultCellEditor(comboBox));
	}

	public AccountBookTableModel getTableModel() {
		return tableModel;
	}

	public JTable getAccountBookTable() {
		return accountBookTable;
	}
}
