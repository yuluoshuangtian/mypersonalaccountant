package com.home.MyPersonalAccountant.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JTable;

import com.home.MyPersonalAccountant.model.AccountBookTableModel;
import com.home.MyPersonalAccountant.model.Category;
import com.home.MyPersonalAccountant.service.DataImportService;
import com.home.MyPersonalAccountant.service.TableDataSelecter;
import com.home.MyPersonalAccountant.service.TableStorageTypeConverter;
import com.home.MyPersonalAccountant.view.ItemAdditionView;
import com.home.MyPersonalAccountant.view.MainPanelTable;
import com.home.MyPersonalAccountant.view.MainPanelView;
import com.home.MyPersonalAccountant.view.ResultPane;
import com.home.MyPersonalAccountant.view.ToolsPanelView;

public class ToolsPanelController implements ActionListener {

	private ToolsPanelView toolsPanelView;
	private DataImportService dataImportService;
	private ResultPane resultPane;

	public ToolsPanelController(ToolsPanelView toolsPanelView) {
		this.toolsPanelView = toolsPanelView;
		this.dataImportService = new DataImportService();
		this.resultPane = new ResultPane();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton) e.getSource();
		if (button.getText() == "Import") {
			importData();
		} else if (button.getText() == "Add") {
			addItem();
		} else if (button.getText() == "Delete") {
			deleteSelectedItems();
		} else if (button.getText() == "Analyze") {
			analyzeData();
		}
	}

	public void importData() {
			AccountBookTableModel tableModel = this.toolsPanelView.getApplicationFrameWindow().getMainPanelTable().getTableModel();
			dataImportService.importData(tableModel);
	}

	public void addItem() {
		new ItemAdditionView(toolsPanelView);
	}

	public void deleteSelectedItems() {
		MainPanelTable mainPanelTable = this.toolsPanelView.getApplicationFrameWindow().getMainPanelTable();
		JTable accountBookTable = mainPanelTable.getAccountBookTable();
		AccountBookTableModel tableModel = mainPanelTable.getTableModel();

		int[] selectedRows = accountBookTable.getSelectedRows();

		if (selectedRows.length > 0) {
			for (int i = selectedRows.length - 1; i >= 0; i--) {
				// Convert the view's row index to the index in the model, and
				// then remove that row from the table model.
				// To do the conversion is because the view is changed after
				// sorting, whilst the model isn't.
				tableModel.removeRow(accountBookTable.convertRowIndexToModel(selectedRows[i]));
			}
		}
	}

	/*
	 * Calculate sum of values from all selected entries and display on the
	 * mainPanelView.
	 */
	public void analyzeData() {
		//TODO: refactoring
		MainPanelView mainPanelView = this.toolsPanelView.getApplicationFrameWindow().getMainPanelView();

		Object[][] selectedDataTable = TableDataSelecter.getSelectData(this.toolsPanelView.getApplicationFrameWindow().getMainPanelTable().getAccountBookTable());
		TableStorageTypeConverter.convert(selectedDataTable);

		BigDecimal sumAll = new BigDecimal("0");
		BigDecimal sumExpense = new BigDecimal("0");

		Map<Category, BigDecimal> expenseOfDifferentCategories = new LinkedHashMap<Category, BigDecimal>();
		Set<Category> categories = new LinkedHashSet<Category>();

		String month = "";
		Map<String, BigDecimal> expenseOfDifferentMonths = new TreeMap<String, BigDecimal>();
		Set<String> months = new TreeSet<String>();
		Calendar calendar = Calendar.getInstance();

		for (int i = 0; i < selectedDataTable.length; i++) {
			Object[] item = selectedDataTable[i];
			sumAll = sumAll.add((BigDecimal) item[2]);
			if (((BigDecimal) item[2]).signum() == -1) {
				sumExpense = sumExpense.add(((BigDecimal) item[2]).negate());

				// Calculate and record the total expense of each category.
				if (categories.add((Category) item[3])) {
					expenseOfDifferentCategories.put((Category) item[3], ((BigDecimal) item[2]).negate());
				} else {
					expenseOfDifferentCategories.put((Category) item[3], expenseOfDifferentCategories.get((Category) item[3]).add(((BigDecimal) item[2]).negate()));
				}

				// Calculate and record the total expense of each month.
				calendar.setTime((Date) item[0]);
				month = (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.YEAR);
				if (months.add(month)) {
					expenseOfDifferentMonths.put(month, ((BigDecimal) item[2]).negate());
				} else {
					expenseOfDifferentMonths.put(month, expenseOfDifferentMonths.get(month).add(((BigDecimal) item[2]).negate()));
				}

			}
			System.out.println((BigDecimal) item[2] + (String) item[1]);
			System.out.println(sumAll);
			System.out.println(sumExpense);
		}

		resultPane.getSumAllNumberLabel().setText("   " + sumAll + " kr");
		resultPane.getSumExpenseNumberLabel().setText("   " + sumExpense + " kr");
		resultPane.getPieData().clear();
		resultPane.getBarData().clear();

		// Set pie dataset.
		for (Map.Entry<Category, BigDecimal> entry : expenseOfDifferentCategories.entrySet()) {
			Category key = entry.getKey();
			BigDecimal value = entry.getValue();
			resultPane.getPieData().setValue(key, value);
		}

		// Set bar dataset.
		for (Map.Entry<String, BigDecimal> entry : expenseOfDifferentMonths.entrySet()) {
			String rowKey = entry.getKey();
			BigDecimal value = entry.getValue();
			resultPane.getBarData().setValue(value, rowKey, "");
		}

		resultPane.revalidate();
		mainPanelView.showResult(resultPane);
	}

}
