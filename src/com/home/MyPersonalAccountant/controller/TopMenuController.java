package com.home.MyPersonalAccountant.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;

public class TopMenuController implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		JMenuItem menuItem = (JMenuItem) e.getSource();
		System.out.println(menuItem.getText());
		if (menuItem.getText() == "About") {
			AboutDialog aboutDialog = new AboutDialog();
			aboutDialog.setVisible(true);
		} else if (menuItem.getText() == "Help") {
			HelpDialog helpDialog = new HelpDialog();
			helpDialog.setVisible(true);
		}
	}
}

class AboutDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private JLabel message1;
	private JLabel message2;
	private JButton btnClose;

	AboutDialog() {

		GroupLayout gl = new GroupLayout(this.getContentPane());
		getContentPane().setLayout(gl);

		message1 = new JLabel("This program do not suck!");
		message2 = new JLabel("It's written by Yustina.tian");

		btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);

		gl.setHorizontalGroup(gl.createSequentialGroup().addGroup(gl.createParallelGroup().addComponent(message1).addComponent(message2).addComponent(btnClose)));
		gl.setVerticalGroup(gl.createSequentialGroup().addGroup(gl.createParallelGroup().addComponent(message1)).addGroup(gl.createParallelGroup().addComponent(message2))
				.addGroup(gl.createParallelGroup().addComponent(btnClose)));

		setTitle("About MyPersonalAccount");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setSize(300, 200);
	}
}

class HelpDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private JLabel message1;
	private JLabel message2;
	private JButton btnClose;

	HelpDialog() {

		GroupLayout gl = new GroupLayout(this.getContentPane());
		getContentPane().setLayout(gl);

		message1 = new JLabel("There's nothing I can help you.");
		message2 = new JLabel("Actually, I'm also confused!");

		btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);

		gl.setHorizontalGroup(gl.createSequentialGroup().addGroup(gl.createParallelGroup().addComponent(message1).addComponent(message2).addComponent(btnClose)));
		gl.setVerticalGroup(gl.createSequentialGroup().addGroup(gl.createParallelGroup().addComponent(message1)).addGroup(gl.createParallelGroup().addComponent(message2))
				.addGroup(gl.createParallelGroup().addComponent(btnClose)));

		setTitle("Help");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setSize(300, 200);
	}
}
