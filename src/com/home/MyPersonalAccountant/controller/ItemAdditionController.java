package com.home.MyPersonalAccountant.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import com.home.MyPersonalAccountant.model.AccountBookTableModel;
import com.home.MyPersonalAccountant.service.DataImportService;
import com.home.MyPersonalAccountant.view.ItemAdditionView;

public class ItemAdditionController implements ActionListener {

	private ItemAdditionView itemAdditionView;

	public ItemAdditionController(ItemAdditionView itemAdditionView) {
		this.itemAdditionView = itemAdditionView;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		JTextField dateField = itemAdditionView.getDateField();
		JTextField categoryField = itemAdditionView.getCategoryField();
		JTextField amountField = itemAdditionView.getAmountField();
		JButton okButton = itemAdditionView.getOkButton();
		
		if (e.getSource() instanceof JTextField) {
			String datePattern = "[0-9]{4}-(\\d){1,2}-(\\d){1,2}";
			String amountPattern = "(-)?[1-9](\\d)*";
			boolean dateMatch = dateField.getText().matches(datePattern);
			boolean amountMatch = amountField.getText().matches(amountPattern);
			if (dateMatch == true && amountMatch == true) {
				okButton.setEnabled(true);
			} else {
				okButton.setEnabled(false);
			}
		}
		if (e.getSource() instanceof JButton) {
			JButton o = (JButton) e.getSource();
			if (o.getText() == "OK") {
				String date = dateField.getText();
				String amount = amountField.getText();
				String category;
				if (categoryField.getText().equals("")) {
					category = "Others";
				} else {
					category = categoryField.getText();
				}
				String[] addedRow = {date, null, amount, category};
				itemAdditionView.getToolsPanelView().getApplicationFrameWindow().getMainPanelTable().getTableModel().addRow(addedRow);
				itemAdditionView.dispose();
				//itemAdditionView.setVisible(false);
			} else if (o.getText() == "Cancel") {
				itemAdditionView.dispose();
			}
		}
		
		if(e.getSource() instanceof JComboBox){
			DataImportService dataImportService = new DataImportService();
			AccountBookTableModel model = itemAdditionView.getToolsPanelView().getApplicationFrameWindow().getMainPanelTable().getTableModel();
			if(((JComboBox<?>)(e.getSource())).getSelectedItem() == "Handelsbanken"){
				dataImportService.importFromHandelsbanken(model);
				itemAdditionView.dispose();
			}
		}

	}

}
