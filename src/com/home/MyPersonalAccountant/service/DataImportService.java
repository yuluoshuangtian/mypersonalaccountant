package com.home.MyPersonalAccountant.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFileChooser;

import com.home.MyPersonalAccountant.model.AccountBookTableModel;
import com.home.MyPersonalAccountant.model.Category;

public class DataImportService {
	
	
	public File getAccountBookFile() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnValue = fileChooser.showOpenDialog(null);

		if (returnValue == JFileChooser.APPROVE_OPTION) {
			return fileChooser.getSelectedFile();
		} else {
			return null;
		}
	}

	public void importData(AccountBookTableModel model) {
		// if file is from handlesbanken
		importFromHandelsbanken(model);
	}

	public void importFromHandelsbanken(AccountBookTableModel model) {
		File file = this.getAccountBookFile();
		if (file != null) {
			try {
				Scanner fileScanner = new Scanner(new FileReader(file));
				while (fileScanner.hasNextLine()) {
					String line = fileScanner.nextLine();
					//TODO: NotePad file has different split "\\t\\t", needs to different NotePad and TextEdit!
					String[] lineData = line.split("\\t \\t");
					List<Object> rowData = new ArrayList<Object>();
					
					for(String s : Arrays.copyOfRange(lineData, 1, 4)){
						rowData.add(s);
					}
					rowData.add(Category.Others);
					model.addRow(rowData.toArray());
				}
				fileScanner.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
