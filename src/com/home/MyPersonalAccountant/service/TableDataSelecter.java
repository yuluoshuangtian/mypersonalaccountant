package com.home.MyPersonalAccountant.service;

import javax.swing.JTable;

public class TableDataSelecter {

	/**
	 * Get user selected entries from JTable and convert to Object[][].
	 * 
	 * @param table
	 * @return
	 */
	public static Object[][] getSelectData(JTable table) {

		int columnCount = table.getColumnCount();
		int selectedRowCount = table.getSelectedRowCount();
		int[] selectedRowIndice = table.getSelectedRows();

		Object[][] selectedData = new Object[selectedRowCount][columnCount];

		int rowIndex = 0;
		for (int selectedRowIndex : selectedRowIndice) {
			for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
				int modelRowIndex = table.convertRowIndexToModel(selectedRowIndex);
				int modelColumnIndex = table.convertColumnIndexToModel(columnIndex);
				selectedData[rowIndex][columnIndex] = table.getModel().getValueAt(modelRowIndex, modelColumnIndex);
			}
			rowIndex++;
		}
		return selectedData;
	}
}
