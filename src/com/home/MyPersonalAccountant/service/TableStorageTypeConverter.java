package com.home.MyPersonalAccountant.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TableStorageTypeConverter {

	/*
	 * I don't like this method to change the argument value inside. Better to
	 * do little refactoring.
	 */
	public static void convert(Object[][] selectedData) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (selectedData != null) {
			for (int i = 0; i < selectedData.length; i++) {
				try {
					// parse data from first column to Date type
					selectedData[i][0] = format.parse((String) selectedData[i][0]);
				} catch (ParseException e) {
					System.err.println("Can not convert to Date type from value: " + selectedData[i][0]);
					e.printStackTrace();
				}
				// parse data from 3rd column to double type
				/*
				 * TODO: This only handles number get from Handelsbanken, should
				 * handle data normalization during input.
				 */
				String cellData = ((String) selectedData[i][2]).replaceAll(",", ".").replaceAll(" ", "");
				selectedData[i][2] = new BigDecimal(cellData);
						//Double.parseDouble(cellData);
			}
		} else {
			System.err.println("no data selected!");
		}
	}
}
