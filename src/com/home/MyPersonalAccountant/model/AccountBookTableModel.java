package com.home.MyPersonalAccountant.model;

import javax.swing.table.DefaultTableModel;

public class AccountBookTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	private String[] columnNames = { "Date", "Business", "Amount", "Category" };
	private Object[][] tableData = {};

	public AccountBookTableModel() {
		this.setDataVector(tableData, columnNames);
	}
	
}
