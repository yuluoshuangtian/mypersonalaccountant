package com.home.MyPersonalAccountant.model;

public enum Category {
	SupermarketAndShop, 
	Restaurant, 
	Entertainment, 
	Housing, 
	Car, 
	PublicTransportation, 
	Fitness, 
	Phone, 
	Friendship, 
	Others;
}
