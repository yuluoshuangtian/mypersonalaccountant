package com.hom.MyPersonalAccountant.service;

import static org.junit.Assert.assertEquals;

import javax.swing.JTable;

import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

import com.home.MyPersonalAccountant.model.AccountBookTableModel;
import com.home.MyPersonalAccountant.service.TableDataSelecter;

public class TableDataSelecterTest {

	private JTable jTable;
	private AccountBookTableModel tableModel;

	@Before
	public void init() {

		jTable = mock(JTable.class);
		tableModel = new AccountBookTableModel();
		
		Object[][] dataVector = {{},{},{}};
		Object[] columnIdentifiers = {"","",""};
		tableModel.setDataVector(dataVector, columnIdentifiers);

		when(jTable.getModel()).thenReturn(tableModel);
		when(jTable.getColumnCount()).thenReturn(3);
		when(jTable.getSelectedRowCount()).thenReturn(3);
		when(jTable.getSelectedRows()).thenReturn(new int[] { 0, 1, 2 });

		when(jTable.convertRowIndexToModel(0)).thenReturn(0);
		when(jTable.convertRowIndexToModel(1)).thenReturn(1);
		when(jTable.convertRowIndexToModel(2)).thenReturn(2);
		
		
	}

	@Test
	public void testGetSelectData() {
		Object[][] selectData = TableDataSelecter.getSelectData(jTable);
		assertEquals(selectData.length, 3);
		assertEquals(selectData[0].length, 3);

	}

}
